package com.example.fileuploaddemo.services

import com.example.fileuploaddemo.models.FileUploadMetadata
import com.example.fileuploaddemo.models.FileUploadResponse
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import org.springframework.web.multipart.MultipartFile
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.RandomAccessFile
import java.nio.file.Path
import java.nio.file.Paths

private const val DELIMITER = " = "

@Service
class FileSystemUploadService(
        @Value("\${example.upload.temp-dir}")
        private val tempDir: String,
        @Value("\${example.upload.dir}")
        private val uploadDir: String
) : UploadService {
    private final val log = LoggerFactory.getLogger(FileSystemUploadService::class.java)

    override fun isChunkUploaded(metadata: FileUploadMetadata): Boolean {
        val filename = this.generateFilenameFromMetadata(metadata)
        val uploadInfo = this.readUploadInfo(filename, metadata.chunkNumber)
        if (uploadInfo != null) {
            val uploadFile = this.getTempUploadFilePath(filename).toFile()

            if (uploadFile.exists() && uploadInfo["chunkSize"] == "${metadata.chunkSize}") {
                return true
            }
        }

        return false
    }

    override fun saveChunk(file: MultipartFile, metadata: FileUploadMetadata): FileUploadResponse {
        val originalFileName = file.originalFilename
        val filename = this.generateFilenameFromMetadata(metadata)
        val tempStorageDirFile = this.getTempStorageDirectoryPath().toFile()
        if (!tempStorageDirFile.exists()) {
            this.log.info("Temp upload directory not found. Creating one now.")
            tempStorageDirFile.mkdirs()
        }
        val tempFilePath = "${this.getTempUploadFilePath(filename)}"
        RandomAccessFile(tempFilePath, "rw").use { tempFile ->
            val chunkPosition = (metadata.chunkNumber - 1) * metadata.chunkSize

            // Save the chunk data
            tempFile.seek(chunkPosition)
            tempFile.write(file.bytes, 0, file.bytes.size)
        }

        // Mark chunk as uploaded
        this.writeUploadInfo(filename, metadata)

        // Do we have all the chunks?
        if (this.isUploadComplete(filename, metadata.totalChunks)) {
            // Move completed file
            val storageDirFile = this.getStorageDirectoryPath().toFile()
            if (!storageDirFile.exists()) {
                this.log.info("Upload directory not found. Creating one now.")
                storageDirFile.mkdirs()
            }
            val tempFile = File(tempFilePath)
            tempFile.renameTo(this.getUploadFilePath(filename).toFile())

            // Clear temp upload info
            this.getUploadInfoDirectoryPath(filename).toFile().deleteRecursively()
        }

        return FileUploadResponse(metadata.filename, metadata.identifier, originalFileName)
    }

    private fun getStorageDirectoryPath(): Path {
        return Paths.get(this.uploadDir, "com.example.fileuploaddemo")
    }

    private fun getTempStorageDirectoryPath(): Path {
        return Paths.get(this.tempDir, "com.example.fileuploaddemo")
    }

    private fun getUploadInfoDirectoryPath(filename: String): Path {
        return Paths.get("${this.getTempStorageDirectoryPath()}", filename)
    }

    private fun generateFilenameFromMetadata(metadata: FileUploadMetadata): String {
        val totalSize = metadata.totalSize
        val chunkSize = metadata.totalSize
        val filename = metadata.filename
        return "$totalSize-$chunkSize-$filename"
    }

    private fun getTempUploadFilePath(filename: String?): Path {
        val uploadDir = this.getTempStorageDirectoryPath()
        return Paths.get("$uploadDir", "$filename.temp")
    }

    private fun getUploadFilePath(filename: String?): Path {
        val uploadDir = this.getStorageDirectoryPath()
        return Paths.get("$uploadDir", "$filename")
    }

    private fun readUploadInfo(filename: String, chunkNumber: Long): Map<String, Any>? {
        val dirPath = this.getUploadInfoDirectoryPath(filename)
        val filePath = Paths.get("$dirPath", "$chunkNumber")
        if (filePath.toFile().exists()) {
            FileReader("$filePath").use { reader ->
                return reader.readLines().associate { line ->
                    val items = line.split(DELIMITER).take(2)
                    Pair(items[0], items[1])
                }
            }
        }

        return null
    }

    private fun writeUploadInfo(filename: String, metadata: FileUploadMetadata) {
        val chunkNumber = metadata.chunkNumber
        val chunkSize = metadata.chunkSize
        val totalChunks = metadata.totalChunks
        val totalSize = metadata.totalSize
        if (chunkNumber < 1 || filename.isBlank()) {
            return
        }

        val dirPath = this.getUploadInfoDirectoryPath(filename)
        if (!dirPath.toFile().exists()) {
            this.log.info("Upload information directory for $filename not found. Creating one now.")
            dirPath.toFile().mkdirs()
        }

        val filePath = Paths.get("$dirPath", "$chunkNumber")

        FileWriter("$filePath", false).use { writer ->
            writer.appendln("chunkNumber$DELIMITER$chunkNumber")
            writer.appendln("chunkSize$DELIMITER$chunkSize")
            writer.appendln("totalChunks$DELIMITER$totalChunks")
            writer.appendln("totalSize$DELIMITER$totalSize")
        }
    }

    private fun isUploadComplete(filename: String, totalChunks: Long): Boolean {
        val dirPath = this.getUploadInfoDirectoryPath(filename)
        if (dirPath.toFile().exists()) {
            val chunkInfo = dirPath.toFile().list { _, fileName ->
                fileName.matches(Regex("[0-9]+"))
            }

            return chunkInfo.size.toLong() == totalChunks
        }

        return false
    }
}