package com.example.fileuploaddemo.models

data class FileUploadResponse(
        val filename: String?,
        val originalFilename: String?,
        val identifier: String?)