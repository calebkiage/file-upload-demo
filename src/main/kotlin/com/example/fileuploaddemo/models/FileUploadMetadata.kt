package com.example.fileuploaddemo.models

import javax.validation.constraints.Min
import javax.validation.constraints.Size

data class FileUploadMetadata(
        @Min(1)
        val chunkSize: Long,
        @Min(1)
        val chunkNumber: Long,
        val totalChunks: Long,
        val totalSize: Long,
        @Size(min = 1)
        val identifier: String,
        @Size(min = 1)
        val filename: String,
        val relativePath: String?) {
    constructor(data: Map<String, String>): this(
            chunkSize = data[CHUNK_SIZE_PARAMETER]?.toLong() ?: 0,
            chunkNumber = data[CHUNK_NUMBER_PARAMETER]?.toLong() ?: 0,
            totalChunks = data[TOTAL_CHUNKS_PARAMETER]?.toLong() ?: 0,
            totalSize = data[TOTAL_SIZE_PARAMETER]?.toLong() ?: 0,
            identifier = data[IDENTIFIER_PARAMETER] ?: "",
            filename = data[FILENAME_PARAMETER] ?: "",
            relativePath = data[RELATIVE_PATH_PARAMETER] ?: ""
    )

    companion object {
        const val CHUNK_SIZE_PARAMETER = "resumableChunkSize"
        const val CHUNK_NUMBER_PARAMETER = "resumableChunkNumber"
        const val TOTAL_CHUNKS_PARAMETER = "resumableTotalChunks"
        const val TOTAL_SIZE_PARAMETER = "resumableTotalSize"
        const val IDENTIFIER_PARAMETER = "resumableIdentifier"
        const val FILENAME_PARAMETER = "resumableFilename"
        const val RELATIVE_PATH_PARAMETER = "resumableRelativePath"
    }
}