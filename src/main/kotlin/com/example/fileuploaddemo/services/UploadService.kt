package com.example.fileuploaddemo.services

import com.example.fileuploaddemo.models.FileUploadMetadata
import com.example.fileuploaddemo.models.FileUploadResponse
import org.springframework.web.multipart.MultipartFile

interface UploadService {
    /**
     * Check if a chunk was already uploaded.
     */
    fun isChunkUploaded(metadata: FileUploadMetadata): Boolean

    /**
     * Save a chunk.
     */
    fun saveChunk(file: MultipartFile, metadata: FileUploadMetadata): FileUploadResponse
}