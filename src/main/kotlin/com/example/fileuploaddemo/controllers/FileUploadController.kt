package com.example.fileuploaddemo.controllers

import com.example.fileuploaddemo.models.FileUploadMetadata
import com.example.fileuploaddemo.models.FileUploadResponse
import com.example.fileuploaddemo.services.UploadService
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.multipart.MultipartFile

@RestController
@RequestMapping("/upload")
@CrossOrigin(origins = ["*"])
class FileUploadController(val uploadService: UploadService) {
    private final val log = LoggerFactory.getLogger(FileUploadController::class.java)

    // Resumable sets the metadata on both the request url as query parameters and on the body as a field in the
    // multipart form data. If we use Spring's default ModelAttribute binding, a weird issue occurs where the values
    // that are strings eg. filename get joined with a comma delimiter. If the file name is file.jpg, the bound value
    // becomes file.jpg,file.jpg
    // See test below with the following resuest:
    // curl --request POST \
    //  --url 'http://localhost:8080/upload/test?chunkNumber=20&totalChunks=2&chunkSize=200&totalSize=30&filename=fdf.jpg&identifier=test&relativePath=' \
    //  --form chunkNumber=20 \
    //  --form totalChunks=2 \
    //  --form chunkSize=200 \
    //  --form totalSize=30 \
    //  --form filename=fdf.jpg \
    //  --form identifier=test \
    //  --form relativePath=
    @PostMapping
    fun uploadFile(@RequestParam("file") file: MultipartFile,
                   @RequestParam parameters: Map<String, String>): ResponseEntity<FileUploadResponse> {
        val response = this.uploadService.saveChunk(file, FileUploadMetadata(parameters))
        return ResponseEntity.ok(response)
    }

    @PostMapping("test")
    fun checkRequest(metadata: FileUploadMetadata?) {
        this.log.info("Gotten parameter: $metadata")
    }

    @GetMapping
    fun checkUpload(@RequestParam parameters: Map<String, String>): ResponseEntity<Any?> {
        return if (this.uploadService.isChunkUploaded(FileUploadMetadata(parameters))) {
            ResponseEntity.status(200).build()
        } else {
            ResponseEntity.status(204).build()
        }
    }
}